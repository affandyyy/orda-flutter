import 'package:flutter/material.dart';
import 'package:Graqlu/screens/history_widget.dart';

class Order extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(children: <Widget>[
          Container(child: topCurveTitle(),),
          Container(
              child: new Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: HistoryWidget(),
          ))
        ]),
      ),
    );
  }

    Widget topCurveTitle({String image, String title}) {
    return Column(
      children: <Widget>[
        Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(color: Colors.white, height: 90.0),
            Container(padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 8.0),child: Text("Your orders",style: TextStyle(color: Colors.white,fontSize: 18.0,fontWeight: FontWeight.w600),),decoration:BoxDecoration(borderRadius:BorderRadius.circular(50.0) ,color: Colors.black87,) )            
          ],
        )
      ],
    );
  }
}
