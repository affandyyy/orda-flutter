import 'package:flutter/material.dart';

class WhatsNewContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 250.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              SizedBox(
                width: 5.0,
              ),
              newsAtGraqlu(
                  image:
                      'https://s3-ap-southeast-1.amazonaws.com/bengkel/wp-content/uploads/2016/08/25181256/kaodim-merdeka-sale.png',
                  title: 'Join us at Kaodim'),
              SizedBox(
                width: 5.0,
              ),
              newsAtGraqlu(
                  image:
                      'https://s3-ap-southeast-1.amazonaws.com/bengkel/wp-content/uploads/2016/08/25181256/kaodim-merdeka-sale.png',
                  title: 'Join us at Kaodim'),
              SizedBox(
                width: 5.0,
              ),
              newsAtGraqlu(
                  image:
                      'https://s3-ap-southeast-1.amazonaws.com/bengkel/wp-content/uploads/2016/08/25181256/kaodim-merdeka-sale.png',
                  title: 'Join us at Kaodim'),
            ],
          ),
        )
      ],
    );
  }

  Widget newsAtGraqlu({String image, String title}) {
    return Column(children: <Widget>[
      SizedBox(width: 5.0),
      Container(
        child: ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Container(
                child: Image.network(image, height: 200.0, fit: BoxFit.cover))),
      ),
      SizedBox(height: 5.0),
      Text(title),
    ]);
  }
}
