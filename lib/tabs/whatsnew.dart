import 'package:Graqlu/screens/whats_new.dart';
import 'package:flutter/material.dart';

class WhatsNew extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(children: <Widget>[
          Container(child: topCurveTitle()),
          Container(
              height: MediaQuery.of(context).size.height / 2,
              child: new Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: WhatsNewContent(),
              )),
        ]),
      ),
    );
  }

  Widget topCurveTitle({String image, String title}) {
    return Column(
      children: <Widget>[
        Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(color: Colors.white, height: 90.0),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
                child: Text(
                  "What's new?",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50.0),
                  color: Colors.black87,
                ))
          ],
        )
      ],
    );
  }
}
