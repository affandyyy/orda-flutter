import 'package:flutter/material.dart';

class SettingsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(color: Colors.white, height: 90.0),
            Container(padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 8.0),child: Text("You",style: TextStyle(color: Colors.white,fontSize: 18.0,fontWeight: FontWeight.w600),),decoration:BoxDecoration(borderRadius:BorderRadius.circular(50.0) ,color: Colors.black87,) )            
          ],
        )
      ],
      
    );
  }
}