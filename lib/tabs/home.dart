import 'dart:async';

import 'package:flutter/material.dart';
import 'package:Graqlu/components/models.dart';
import 'package:Graqlu/components/photo_scroller.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:Graqlu/components/theme.dart';

class Home extends StatelessWidget {
  Home(this.content);
  final Content content;

  @override
  Widget build(BuildContext context) {
    // var textTheme = Theme.of(context).textTheme;

    return Scaffold(
      body: Stack(
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: <Widget>[
            MapSample(),
            SafeArea(
                child: PhotoScroller(content.photoUrls, content.namaKedai)),
          ]),
      bottomSheet: new Container(
        height: 60.0,
        color: Colors.transparent,
        child: new Container(
          decoration: BoxDecoration(
              borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(40.0),
                  topRight: const Radius.circular(40.0))),
          child: new ListTile(
            leading: Icon(FontAwesomeIcons.wallet),
            title: Text("Credit Balance"),
            trailing: Text("199.90", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
          ),
        ),
      ),

      // Column(
      //   children: <Widget>[
      //     Expanded(
      //       flex: 3,
      //       child: Stack(
      //         alignment: Alignment.center,
      //         overflow: Overflow.visible,
      //         children: <Widget>[
      //           Positioned(
      //             top: 50.0,
      //             child: Container(
      //                 padding: EdgeInsets.symmetric(
      //                     horizontal: 20.0, vertical: 8.0),
      //                 child: Text(
      //                   "Current Balance",
      //                   style: TextStyle(
      //                       color: Colors.white,
      //                       fontSize: 18.0,
      //                       fontWeight: FontWeight.w600),
      //                 ),
      //                 decoration: BoxDecoration(
      //                   borderRadius: BorderRadius.circular(50.0),
      //                   color: Colors.black87,
      //                 )),
      //           ),
      //           // SizedBox(height: 30.0),
      //         ],
      //       ),
      //     ),
      //     Expanded(
      //         flex: 1,
      //         child: Container(
      //           height: MediaQuery.of(context).size.height * 0.25,
      //           child: Column(
      //             mainAxisAlignment: MainAxisAlignment.start,
      //             crossAxisAlignment: CrossAxisAlignment.start,
      //             children: <Widget>[
      //               // Padding(
      //               //   padding: const EdgeInsets.only(),
      //               //   child: Text(
      //               //     'What\'s new on your area',
      //               //     style: TextStyle(color: Color(GraqluColor.theGrey)),
      //               //   ),
      //               // ),
      //               PhotoScroller(content.photoUrls, content.namaKedai),
      //             ],
      //           ),
      //         ))
      //   ],
      // ),
    );
  }
}

// child: SingleChildScrollView(
//           child: Column(
//             children: [
//               Text(
//                 'Tops',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//               Text(
//                 'Bottom',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//               Text(
//                 'Shoes',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//               Text(
//                 'Accessories',
//                 style: textTheme.subhead.copyWith(fontSize: 18.0),
//               ),
//               PhotoScroller(content.photoUrls),
//               SizedBox(height: 20.0),
//             ],
//           ),
//         ),

// class HeaderColor extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     var path = new Path();

//     path.lineTo(0.0, size.height);
//     path.lineTo(size.width, size.height);
//     path.lineTo(size.width, 0.0);
//     path.close();
//     return path;
//   }

//   @override
//   bool shouldReclip(CustomClipper<Path> oldClipper) => false;
// }

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        child: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _kGooglePlex,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),
      ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}
