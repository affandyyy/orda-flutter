import 'package:flutter/material.dart';

class PedlarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 4,
            decoration: BoxDecoration(
                // color: Colors.blue,
                image: DecorationImage(
                    image: NetworkImage(
                        'https://s-ec.bstatic.com/data/xphoto/1182x887/209/20963241.jpg?size=S'),
                    fit: BoxFit.cover)),
          ),
          Column(
            // padding: EdgeInsets.symmetric(horizontal: 10.0),
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 100.0, left: 25.0, right: 5.0),
                child: Card(
                  elevation: 2.0,
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.arrow_back),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            Text("Ali Cendol",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold)),
                            IconButton(
                              icon: Icon(Icons.star_border),
                              onPressed: () {},
                            )
                          ],
                        ),
                        // Container(
                        //     padding: EdgeInsets.fromLTRB(20.0, 0, 20.0, 16.0),
                        //     child: Text(
                        //       "State-of-the-art cendol in town, you will never unsatisfied",
                        //       textAlign: TextAlign.justify,
                        //     ))
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 30.0),
              Container(
                height: 150.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    SizedBox(
                      width: 20.0,
                    ),
                    _buildPlaceToVisit(
                        image:
                            'https://media.timeout.com/images/103572212/630/472/image.jpg',
                        title: "Pashupatinath"),
                    SizedBox(
                      width: 20.0,
                    ),
                    _buildPlaceToVisit(
                        image:
                            'https://media.timeout.com/images/103572212/630/472/image.jpg',
                        title: "Swoyambhunath"),
                    SizedBox(
                      width: 20.0,
                    ),
                    _buildPlaceToVisit(
                        image:
                            'https://media.timeout.com/images/103572212/630/472/image.jpg',
                        title: "Durbar Square"),
                    SizedBox(
                      width: 20.0,
                    ),
                    _buildPlaceToVisit(
                        image:
                            'https://media.timeout.com/images/103572212/630/472/image.jpg',
                        title: "Pashupatinath"),
                    SizedBox(
                      width: 20.0,
                    ),
                    _buildPlaceToVisit(
                        image:
                            'https://media.timeout.com/images/103572212/630/472/image.jpg',
                        title: "Swoyambhunath"),
                  ],
                ),
              ),
              SizedBox(height: 30.0),
              Expanded(
                flex: 3,
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  children: <Widget>[
                    ListTile(
                      onTap: () {},
                      // leading: Icon(Icons.edit),
                      leading: Text("Cendol Biasa"),
                      trailing: Text("RM2"),
                    ),
                    ListTile(
                      onTap: () {},
                      // leading: Icon(Icons.favorite_border),
                      leading: Text("Cendol Tapai"),    
                      trailing: Text("RM2"),

                    ),
                    ListTile(
                      onTap: () {},
                      // leading: Icon(Icons.account_balance_wallet),
                      leading: Text("Cendol Pulut"),
                      trailing: Text("RM2"),

                    ),
                    ListTile(
                      onTap: () {},
                      // leading: Icon(Icons.settings),
                      leading: Text("Cendol Jagung"),
                      trailing: Text("RM2"),

                      
                    ),
                  ],
                ),
              )
              // Container(
              //   height: 250,
              //   child: Row(
              //     children: <Widget>[
              //       Expanded(child: ClipRRect(
              //         borderRadius: BorderRadius.circular(5.0),
              //         child: Image.network('https://ucarecdn.com/b59dcb57-979c-4742-9d60-e71c74c0841e/-/scale_crop/1280x720/center/-/quality/lighter/', height: 180, fit: BoxFit.cover))),
              //       SizedBox(width: 20.0,),
              //       Expanded(
              //         child: GridView(
              //           shrinkWrap: true,
              //           gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              //             crossAxisCount: 2,
              //             crossAxisSpacing: 10.0,
              //             mainAxisSpacing: 10.0
              //           ),
              //           children: <Widget>[
              //             ClipRRect(
              //               borderRadius: BorderRadius.circular(5.0),
              //               child: Image.network('https://ucarecdn.com/b59dcb57-979c-4742-9d60-e71c74c0841e/-/scale_crop/1280x720/center/-/quality/lighter/',fit: BoxFit.cover)),
              //             ClipRRect(
              //               borderRadius: BorderRadius.circular(5.0),
              //               child: Image.network('https://ucarecdn.com/b59dcb57-979c-4742-9d60-e71c74c0841e/-/scale_crop/1280x720/center/-/quality/lighter/',fit: BoxFit.cover)),
              //             ClipRRect(
              //               borderRadius: BorderRadius.circular(5.0),
              //               child: Image.network('https://ucarecdn.com/b59dcb57-979c-4742-9d60-e71c74c0841e/-/scale_crop/1280x720/center/-/quality/lighter/',fit: BoxFit.cover)),
              //             ClipRRect(
              //               borderRadius: BorderRadius.circular(5.0),
              //               child: Image.network('https://ucarecdn.com/b59dcb57-979c-4742-9d60-e71c74c0841e/-/scale_crop/1280x720/center/-/quality/lighter/',fit: BoxFit.cover)),
              //           ],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildPlaceToVisit({String image, String title}) {
    return Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Image.network(
            image,
            height: 120.0,
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        Text(title)
      ],
    );
  }
}
