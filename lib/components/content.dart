import 'package:Graqlu/components/models.dart';

final Content testMovie = Content(
  bannerUrl: 'images/banner.png',
  posterUrl: 'images/poster.png',
  title: 'The Secret Life of Pets',
  rating: 8.0,
  starRating: 4,
  categories: ['Animation', 'Comedy'],
  storyline: 'For their fifth fully-animated feature-film '
      'collaboration, Illumination Entertainment and Universal '
      'Pictures present The Secret Life of Pets, a comedy about '
      'the lives our...',
  photoUrls: [
    'images/1.jpg',
    'images/2.jpg',
    'images/3.jpg',
    'images/4.png',
    'images/1.png',
  ],
  namaKedai:[
    'Ali Cendol',
    'Bistro Q',
    'Warung Bunian',
    'ABC Power',
    'Laksa Johor'
  ]
);
