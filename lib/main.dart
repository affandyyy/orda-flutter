import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import './tabs/home.dart' as _firstTab;
import './tabs/order.dart' as _orderTab;
import './tabs/settings.dart' as _settingsTab;
import './tabs/whatsnew.dart' as _whatsNew;
import './screens/about.dart' as _aboutPage;
import './screens/support.dart' as _supportPage;
import 'package:Graqlu/components/content.dart';
import 'package:splashscreen/splashscreen.dart';


void main() => runApp(new MaterialApp(
      title: 'Graqlu',
      theme: new ThemeData(
          // fontFamily: 'Quicksand',
          primarySwatch: Colors.blueGrey,
          scaffoldBackgroundColor: Colors.white,
          primaryColor: Colors.blueGrey,
          backgroundColor: Colors.white),
      home: new SplashScreen(
          seconds: 4,
          navigateAfterSeconds: new Tabs(),
          title: new Text(
            '',
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
          ),
          image: new Image.asset('images/icon_grey.png'),
          // backgroundColor: Color(0xffF23E3E),
          gradientBackground: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [const Color(0xffF23E3E), const Color(0xff990033)],
          ),
          styleTextUnderTheLoader: new TextStyle(),
          photoSize: 50.0,
          // onClick: ()=>print("Flutter Egypt"),
          // loadingText: Text('No great food left behind', style: TextStyle(color: Colors.white, fontFamily: 'Quicksand'),),
          loaderColor: Colors.white),
      // home: new Tabs(),

      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/about':
            return new FromRightToLeft(
              builder: (_) => new _aboutPage.About(),
              settings: settings,
            );
          case '/support':
            return new FromRightToLeft(
              builder: (_) => new _supportPage.Support(),
              settings: settings,
            );
        }
      },
      // routes: <String, WidgetBuilder> {
      //   '/about': (BuildContext context) => new _aboutPage.About(),
      // }
    ));

class FromRightToLeft<T> extends MaterialPageRoute<T> {
  FromRightToLeft({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;

    return new SlideTransition(
      child: new Container(
        decoration: new BoxDecoration(boxShadow: [
          new BoxShadow(
            color: Colors.black26,
            blurRadius: 25.0,
          )
        ]),
        child: child,
      ),
      position: new Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: Offset.zero,
      ).animate(new CurvedAnimation(
        parent: animation,
        curve: Curves.fastOutSlowIn,
      )),
    );
  }

  @override
  Duration get transitionDuration => const Duration(milliseconds: 400);
}

class Tabs extends StatefulWidget {
  @override
  TabsState createState() => new TabsState();
}

class TabsState extends State<Tabs> {
  PageController _tabController;

  var _title_app = null;
  int _tab = 0;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _tabController = new PageController();
    this._title_app = TabItems[0].title;
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    return Scaffold(
      key: _scaffoldKey,
      //App Bar
      // appBar: new AppBar(
      //   title: new Text(
      //     _title_app,
      //     style: new TextStyle(
      //       color: Colors.black,
      //       fontSize:
      //           Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
      //     ),
      //   ),
      //   // leading: new Icon(Icons.person, color: Colors.grey),
      //   // onPressed: () => _scaffoldKey.currentState.openDrawer()

      //   elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      //   backgroundColor: Colors.transparent,
      //   actions: <Widget>[
      //       // action button
      //       IconButton(
      //         icon: Icon(Icons.notifications),
      //         color: Colors.grey,
      //         onPressed: () {
      //           // _select(choices[0]);
      //         },
      //       ),

      //     ],
      // ),

      //Content of tabs
      body: new PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _tabController,
        onPageChanged: onTabChanged,
        children: <Widget>[
          new _firstTab.Home(testMovie),
          new _whatsNew.WhatsNew(),
          new _orderTab.Order(),
          new _settingsTab.Settings(),
        ],
      ),

      //Tabs
      bottomNavigationBar: Theme.of(context).platform == TargetPlatform.iOS
          ? new CupertinoTabBar(
              activeColor: Color(0xff990033),
              currentIndex: _tab,
              onTap: onTap,
              items: TabItems.map((TabItem) {
                return new BottomNavigationBarItem(
                  title: new Text(TabItem.title),
                  icon: new Icon(TabItem.icon),
                );
              }).toList(),
            )
          : new BottomNavigationBar(
              currentIndex: _tab,
              onTap: onTap,
              items: TabItems.map((TabItem) {
                return new BottomNavigationBarItem(
                  title: new Text(TabItem.title),
                  icon: new Icon(TabItem.icon),
                );
              }).toList(),
            ),

      //Drawer
      // drawer: new Drawer(
      //     child: new ListView(
      //   children: <Widget>[
      //     new Container(
      //       height: 120.0,
      //       child: new DrawerHeader(
      //         padding: new EdgeInsets.all(0.0),
      //         decoration: new BoxDecoration(
      //           color: new Color(0xFFECEFF1),
      //         ),
      //         child: new Center(
      //           child: new FlutterLogo(
      //             colors: Colors.blueGrey,
      //             size: 54.0,
      //           ),
      //         ),
      //       ),
      //     ),
      //     new ListTile(
      //         leading: new Icon(Icons.chat),
      //         title: new Text('Support'),
      //         onTap: () {
      //           Navigator.pop(context);
      //           Navigator.of(context).pushNamed('/support');
      //         }),
      //     new ListTile(
      //         leading: new Icon(Icons.info),
      //         title: new Text('About'),
      //         onTap: () {
      //           Navigator.pop(context);
      //           Navigator.of(context).pushNamed('/about');
      //         }),
      //     new Divider(),
      //     new ListTile(
      //         leading: new Icon(Icons.exit_to_app),
      //         title: new Text('Sign Out'),
      //         onTap: () {
      //           Navigator.pop(context);
      //         }),
      //   ],
      // ))
    );
  }

  void onTap(int tab) {
    _tabController.jumpToPage(tab);
  }

  void onTabChanged(int tab) {
    setState(() {
      this._tab = tab;
    });

    switch (tab) {
      case 0:
        this._title_app = TabItems[0].title;
        break;

      case 1:
        this._title_app = TabItems[1].title;
        break;

      case 2:
        this._title_app = TabItems[2].title;
        break;

      case 3:
        this._title_app = TabItems[3].title;
        break;
    }
  }
}

class TabItem {
  const TabItem({this.title, this.icon});
  final String title;
  final IconData icon;
}

const List<TabItem> TabItems = const <TabItem>[
  const TabItem(title: 'Around', icon: Icons.near_me),
  const TabItem(title: 'What\'s new', icon: Icons.whatshot),
  const TabItem(title: 'Orders', icon: Icons.check_box_outline_blank),
  const TabItem(title: 'You', icon: Icons.panorama_fish_eye)
];
