import 'package:flutter/material.dart';
import 'package:Graqlu/components/setting_widget.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: <Widget>[
          Container(
            child: topCurveTitle(),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Hi, John Appleseed",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold)),
                    Text(
                      "Craving for Nasi Lemak?",
                      style: TextStyle(color: Colors.grey.shade700),
                    )
                  ],
                ),
                CircleAvatar(
                  backgroundImage: AssetImage('images/ab.png'),
                  radius: 40,
                )
              ],
            ),
          ),
          Expanded(
            child: ListView(
              // padding: EdgeInsets.all(10.0),
              children: <Widget>[
                ListTile(
                  onTap: () {},
                  leading: Icon(Icons.edit),
                  title: Text("My Orders"),
                ),
                ListTile(
                  onTap: () {},
                  leading: Icon(Icons.favorite_border),
                  title: Text("My Favorite"),
                ),
                ListTile(
                  onTap: () {},
                  leading: Icon(Icons.account_balance_wallet),
                  title: Text("My Credits"),
                ),
                ListTile(
                  onTap: () {},
                  leading: Icon(Icons.settings),
                  title: Text("My Account"),
                ),
              ],
            ),
          )
        ],
      ),
    ));
  }

  Widget topCurveTitle({String image, String title}) {
    return Column(
      children: <Widget>[
        Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(color: Colors.white, height: 90.0),
            Container(padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 8.0),child: Text("You",style: TextStyle(color: Colors.white,fontSize: 18.0,fontWeight: FontWeight.w600),),decoration:BoxDecoration(borderRadius:BorderRadius.circular(50.0) ,color: Colors.black87,) )            
          ],
        )
      ],
    );
  }
}
