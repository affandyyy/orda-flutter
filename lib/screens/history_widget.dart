
import 'package:flutter/material.dart';
import 'package:Graqlu/screens/payment_widget.dart';
import 'package:Graqlu/mockdata/database.dart' as db;
// import 'package:payment/challenge0/widgets/payment_widget.dart';
// import 'package:payment/challenge0/resources/database.dart' as db;

class HistoryWidget extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[

        Container(
          color: Colors.white,
          child: ListView.builder(
            itemBuilder: (context,index){
              return PaymentWidget(db.getPayments()[index]);
            },
            itemCount: db.getPayments().length,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
          ),
        ),
      ],
    );
  }
  
}